
public class Scanner {

	public Bag scanBag(Bag bag) {
		bag.suspicious = false;
		bag.clean = true;
		return bag;
	}
}
